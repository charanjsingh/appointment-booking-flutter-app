import 'dart:async';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>['email'],
);

Future<GoogleSignInAccount> handleSignIn() async {
  try {
    await _googleSignIn.signIn();
    return _googleSignIn.currentUser;
  } catch (error) {
    print(error);
  }
}

Future<void> _handleSignOut() async {
  _googleSignIn.disconnect();
}