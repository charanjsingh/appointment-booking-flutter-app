/**
 * Module that handles all user authentications
 */
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

final _storageKeyLoggedIn = "user_logged_in";
final _storageKeyUserName = "user_name";
final _storageKeyUserMobile = "user_mobile";

class UserDetail {
  String name;
  String email;
  String mobile;
}
UserDetail loggedInUser = new UserDetail();

Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

/// ----------------------------------------------------------
/// Method that saves the token in Shared Preferences
/// ----------------------------------------------------------
Future<bool> setLoggedIn({String name, String mobile}) async {
  final SharedPreferences prefs = await _prefs;

  prefs.setBool(_storageKeyLoggedIn, true);
  prefs.setString(_storageKeyUserName, name);
  return prefs.setString(_storageKeyUserMobile, mobile);
}


/// ----------------------------------------------------------
/// Method that saves the token in Shared Preferences
/// ----------------------------------------------------------
Future<bool> setLoggedOut() async {
  final SharedPreferences prefs = await _prefs;

  prefs.remove(_storageKeyLoggedIn);
  prefs.remove(_storageKeyUserName);
  return prefs.remove(_storageKeyUserMobile);
}


/// ----------------------------------------------------------
/// Return logged in status
/// ----------------------------------------------------------
Future<dynamic> isLoggedIn() async {
  final SharedPreferences prefs = await _prefs;

  bool _isLoggedIn =  prefs.getBool(_storageKeyLoggedIn) ?? false;
  if(_isLoggedIn){
    String _userName =  prefs.getString(_storageKeyUserName) ?? '';
    String _userMobile =  prefs.getString(_storageKeyUserMobile) ?? '';
    loggedInUser.name = _userName;
    loggedInUser.mobile = _userMobile;
    loggedInUser.email = "";
    return loggedInUser;
  }
  return false;
}
