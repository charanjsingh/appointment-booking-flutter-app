import 'package:flutter/material.dart';

class MyBottomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BottomAppBar(
        color: Colors.blue,
        elevation: 0.0,
        child: FlatButton(
            onPressed: () => Navigator.popAndPushNamed(context, '/book_appointment'),
            child: Text("Book Appointment")
        )
    );
  }
}