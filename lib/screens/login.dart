import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:validate/validate.dart';

import 'package:flutter_app/req/google_auth.dart' as GoogleAuth;
import 'package:flutter_app/req/my_http_requests.dart';
import 'package:flutter_app/req/my_authenticate_requests.dart';
import 'package:flutter_app/req/globals.dart' as globals;
import 'package:flutter_app/screens/my_app_drawer.dart';

bool loginInProgress = false;

// LOGIN PAGE TO SELECT WHERE TO LOGIN FROM
class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginState();
}

class _LoginState extends State<Login> {
  bool facebookLoginInProgress = false;
  bool googleLoginInProgress = false;
  final GlobalKey<ScaffoldState> _scaffoldKey =  new GlobalKey<ScaffoldState>();

  void initiateFacebookLogin() async {
    this.setState(() => facebookLoginInProgress = true );
    var facebookLogin = FacebookLogin();
    var facebookLoginResult =
    await facebookLogin.logInWithReadPermissions(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("Error");
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        break;
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn");
        var graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture&access_token=${facebookLoginResult
                .accessToken.token}');

        var profile = json.decode(graphResponse.body);
        print(profile.toString());
        Map data = {
          "channel": "facebook",
          "id": profile["id"],
          "name": profile["name"],
          "email": profile["email"],
          "mobile" : ""
        };
        String response = await httpPost("social_login.php", data);
        Map responseData = json.decode(response);
        if (responseData["success"] == "1") {
          String _responseName = responseData["details"]["name"];
          if(responseData["details"]["new_user"] == 1){
            setLoggedIn(name: _responseName).then((bool done) {
              globals.isLoggedIn = true;
              globals.loggedInUserMobile = "Facebook";
              loginInProgress = true;
              Navigator.push(context, MaterialPageRoute(builder: (context) => MyProfile()) );
            });
          }
          else{
            setLoggedIn(name: _responseName).then((bool done) {
              globals.isLoggedIn = true;
              globals.loggedInUserName = _responseName;
              globals.loggedInUserMobile = "Facebook";
              Navigator.of(context).pushNamed("/home");
            });
          }
        } else {
          _scaffoldKey.currentState.showSnackBar(
              SnackBar(
                  content: Text(responseData["message"])
              )
          );
        }
        break;
    }
    this.setState(() => facebookLoginInProgress = false );
  }

  void initiateGoogleLogin() async {
    this.setState(() => googleLoginInProgress = true );
    GoogleAuth.handleSignIn().then((_currentUser) async {
      print(_currentUser.displayName);
      Map data = {
        "channel": "google",
        "id": _currentUser.id,
        "name": _currentUser.displayName,
        "email": _currentUser.email,
        "mobile" : ""
      };
      String response = await httpPost("social_login.php", data);
      Map responseData = json.decode(response);
      if (responseData["success"] == "1") {
        String _responseName = responseData["details"]["name"];
        if(responseData["details"]["new_user"] == 1){
          setLoggedIn(name: _responseName).then((bool done) {
            globals.isLoggedIn = true;
            globals.loggedInUserMobile = "Google";
            loginInProgress = true;
            Navigator.push(context, MaterialPageRoute(builder: (context) => MyProfile()) );
          });
        }
        else{
          setLoggedIn(name: _responseName).then((bool done) {
            globals.isLoggedIn = true;
            globals.loggedInUserName = _responseName;
            globals.loggedInUserMobile = "Google";
            Navigator.of(context).pushNamed("/home");
          });
        }
      } else {
        _scaffoldKey.currentState.showSnackBar(
            SnackBar(
                content: Text(responseData["message"])
            )
        );
      }
      this.setState(() => googleLoginInProgress = false );
    });
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = TextStyle(fontSize: 18.0, color: Colors.white);

    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        elevation: 0.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 13.0, top: 13.0),
              child: InkWell(
                child: Text(
                  "Skip Login",
                  style: textStyle,
                ),
                onTap: () {
                  Navigator.of(context).pushNamed('/home');
                },
              ),
            )
          ]
        )
      ),
      body: Container(
          alignment: AlignmentDirectional.center,
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(FontAwesomeIcons.fire, size: 140.0, color: Colors.white),
                Padding(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: Text("Sign in with:", style: textStyle),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(15.0),
                      child: SizedBox(
                        width: 80.0,
                        height: 80.0,
                        child: OutlineButton(
                            child: facebookLoginInProgress ?
                            CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white)
                            ) :
                            Icon(FontAwesomeIcons.facebookF, color: Colors.white),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(40.0)),
                            onPressed: () => initiateFacebookLogin()
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15.0, right: 15.0),
                      child: Text(
                        "OR",
                        style: TextStyle(
                          color: Colors.white54,
                          fontSize: 22.0,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(15.0),
                      child: SizedBox(
                        width: 80.0,
                        height: 80.0,
                        child: OutlineButton(
                            child: googleLoginInProgress ?
                            CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white)
                            ) :
                            Icon(FontAwesomeIcons.google, color: Colors.white),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(40.0)),
                            onPressed: () => initiateGoogleLogin()
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: SizedBox(
                    width: 250.0,
                    height: 60.0,
                    child: OutlineButton(
                        child: Text("Login With Number", style: textStyle),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GetMobileNumber()),
                          );
                        }),
                  ),
                ),
              ],
            ),
          )
      ),
    );
  }
}

// LOGIN FROM MOBILE NUMBER
class GetMobileNumber extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _GetMobileNumberState();
}

class _GetMobileNumberState extends State<GetMobileNumber> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _inputNumber = "";
  bool isLoading = false;

  // VALIDATION FUNCTION FOR PHONE NUMBER
  String _validateMobileNumber(String value) {
    if (value.length != 10) return 'Enter a 10 digit mobile number';
    return null;
  }

  void submit() async {
    if (this._formKey.currentState.validate()) {
      this.setState(() => isLoading = true );
      _formKey.currentState.save();
      Map data = {"number": _inputNumber,};
      String response = await httpPost("send_otp.php", data);
      print(response);
      Map responseData = json.decode(response);
      this.setState(() => isLoading = false );
      if (responseData["success"] == "1") {
        Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOTP()) );
      } else {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(responseData["message"])
          )
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("Login With Number"), elevation: 0.0,),
      drawer: MyAppDrawer(),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: AlignmentDirectional.center,
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(color: Colors.blue),
              child: Icon(FontAwesomeIcons.fire, size: 140.0, color: Colors.white),
            ),
          ),
          Expanded(
            child: Container(
              alignment: AlignmentDirectional.center,
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(color: Colors.white),
              child: isLoading
                  ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                        child: Text("Sending OTP...")
                      )
                    ],
                  )
                  : Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      TextFormField(
                        keyboardType: TextInputType.phone,
                        validator: this._validateMobileNumber,
                        initialValue: _inputNumber,
                        onSaved: (String value) => _inputNumber = value,
                        decoration: InputDecoration(
                            hintText: "Number without +91",
                            labelText: "Enter Your Number",
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 13.0),
                        child: RaisedButton(
                          color: Colors.blue,
                          onPressed: () => this.submit(),
                          child: Text(
                            "Send OTP",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            ),
          )
        ],
        )
    );
  }
}

// VERIFY OTP IF LOGIN FROM MOBILE NUMBER
class VerifyOTP extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _VerifyOTPState();
}

class _VerifyOTPState extends State<VerifyOTP> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _inputOtp = "";
  bool isLoading = false;

  // VALIDATION FUNCTION FOR PHONE NUMBER
  String _validateOTP(String value) {
    if (value.length != 6) return 'Enter a 6 digit OTP';
    return null;
  }

  void submit() async {
    if (this._formKey.currentState.validate()) {
      this.setState(() => isLoading = true );
      _formKey.currentState.save();
      Map data = {"otp": _inputOtp,};
      String response = await httpPost("verify_otp.php", data);
      print(response);
      Map responseData = json.decode(response);
      this.setState(() => isLoading = false );
      if (responseData["success"] == "1") {
        String _responseName = responseData["details"]["name"];
        String _responseMobile = responseData["details"]["mobile"];
        if(responseData["details"]["new_user"] == 1){
          setLoggedIn(mobile: _responseMobile).then((bool done) {
            globals.isLoggedIn = true;
            globals.loggedInUserMobile = _responseMobile;
            loginInProgress = true;
            Navigator.push(context, MaterialPageRoute(builder: (context) => MyProfile()) );
          });
        }
        else{
          setLoggedIn(name: _responseName, mobile: _responseMobile).then((bool done) {
            globals.isLoggedIn = true;
            globals.loggedInUserName = _responseName;
            globals.loggedInUserMobile = _responseMobile;
            Navigator.of(context).pushNamed("/home");
          });
        }
      } else {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(responseData["message"])
          )
        );
      }
    }
  }

  void resendOtp() async {
      this.setState(() => isLoading = true );
      String response = await httpGet("resend_otp.php");
      print(response);
      Map responseData = json.decode(response);
      this.setState(() {isLoading = false; _inputOtp = "";});
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(responseData["message"])
        )
      );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("Enter OTP")),
      drawer: MyAppDrawer(),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: AlignmentDirectional.center,
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(color: Colors.blue),
              child: Icon(FontAwesomeIcons.fire, size: 140.0, color: Colors.white),
            ),
          ),
          Expanded(
            child: Container(
              alignment: AlignmentDirectional.center,
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(color: Colors.white),
              child: isLoading
                  ? Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Text("Verifying OTP...")
                  )
                ],
              )
                  : Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      TextFormField(
                        initialValue: _inputOtp,
                        validator: this._validateOTP,
                        onSaved: (String value) => _inputOtp = value,
                        decoration: InputDecoration(
                            hintText: "6 Digit OTP",
                            labelText: "Enter OTP",
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 13.0),
                        child: RaisedButton(
                          color: Colors.blue,
                          onPressed: () => this.submit(),
                          child: Text(
                            "Verify OTP",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 13.0),
                        child: RaisedButton(
                          color: Colors.blue,
                          onPressed: () => this.resendOtp(),
                          child: Text(
                            "Resend OTP",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            ),
          )
        ],
      )
    );
  }
}

// PROFILE PAGE
class MyProfile extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  static final TextEditingController _controllerName = new TextEditingController();
  static final TextEditingController _controllerMobile = new TextEditingController();
  static final TextEditingController _controllerEmail = new TextEditingController();
  String get _inputName => _controllerName.text;
  String get _inputMobile => _controllerMobile.text;
  String get _inputEmail => _controllerEmail.text;
  final FocusNode _focusName = FocusNode();
  final FocusNode _focusMobile = FocusNode();
  final FocusNode _focusEmail = FocusNode();
  UserDetail currentUser;
  bool isLoading = false;
  String loadingMessage = "";

  // VALIDATION STARTS HERE
  String _validateName(String value){
    if(value.isEmpty) return "Name is required";
    return null;
  }

  String _validateMobile(String value){
    if(value.length != 10) return "Mobile no should be 10 digit";
    return null;
  }

  String _validateEmail(String value){
    try{
      Validate.isEmail(value);
      return null;
    }
    catch(e){
      return "Not a valid email";
    }
  }
  // VALIDATION ENDS HERE

  void submit() async {
    if(_formKey.currentState.validate()){
      _formKey.currentState.save();
      this.setState(() { loadingMessage = "Saving profile..."; isLoading = true; });
      Map data = {"name": _inputName,"email": _inputEmail, "mobile" : _inputMobile};
      String response = await httpPost("save_profile.php", data);
      Map responseData = json.decode(response);
      this.setState(() { loadingMessage = ""; isLoading = false; });
      if (responseData["success"] == "1") {
        setLoggedIn(name: _inputName, mobile: _inputMobile).then((bool done) {
          globals.loggedInUserName = _inputName;
          globals.loggedInUserMobile = _inputMobile;
          if(loginInProgress){
            loginInProgress = false;
            Navigator.of(context).pushNamed("/home");
          }
          else{
            _scaffoldKey.currentState.showSnackBar(
                SnackBar(
                    content: Text("Profile Saved")
                )
            );
          }
        });
      } else {
        _scaffoldKey.currentState.showSnackBar(
            SnackBar(
                content: Text(responseData["message"])
            )
        );
      }
    }
  }

  @override
  void initState() {
    super.initState();
    if(loginInProgress){
      isLoggedIn().then((userData){
        if(userData != false){
          _controllerName.text = userData.name;
          _controllerMobile.text = userData.mobile;
          _controllerEmail.text = userData.email;
        }
      });
    }
    else{
      this.setState(() { loadingMessage = "Fetching profile details..."; isLoading = true; });
      httpGet("get_profile.php").then((String response){
        Map responseData = json.decode(response);
        if (responseData["success"] == "1") {
          _controllerName.text = responseData["details"]["name"];
          _controllerMobile.text = responseData["details"]["mobile"];
          _controllerEmail.text = responseData["details"]["email"];
        } else {
          _scaffoldKey.currentState.showSnackBar(
              SnackBar(
                  content: Text(responseData["message"])
              )
          );
        }
        this.setState(() { loadingMessage = ""; isLoading = false; });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("My Profile")),
      drawer: MyAppDrawer(),
      body: Container(
          alignment: AlignmentDirectional.center,
          padding: EdgeInsets.all(15.0),
          child: isLoading ?
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: Text(loadingMessage)
                )
              ],
            )
            : Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                TextFormField(
                  controller: _controllerName,
                  textInputAction: TextInputAction.next,
                  focusNode: _focusName,
                  validator: this._validateName,
                  onFieldSubmitted: (term) => FocusScope.of(context).requestFocus(_focusMobile),
                  decoration: InputDecoration(
                    labelText: "Name",
                  ),
                ),
                TextFormField(
                  controller: _controllerMobile,
                  textInputAction: TextInputAction.next,
                  focusNode: _focusMobile,
                  validator: _validateMobile,
                  onFieldSubmitted: (term) => FocusScope.of(context).requestFocus(_focusEmail),
                  decoration: InputDecoration(
                    labelText: "Mobile",
                  ),
                ),
                TextFormField(
                  controller: _controllerEmail,
                  textInputAction: TextInputAction.done,
                  focusNode: _focusEmail,
                  validator: _validateEmail,
                  onFieldSubmitted: (term) => this.submit(),
                  decoration: InputDecoration(
                    labelText: "Email",
                  ),
                ),
                RaisedButton(
                  child: Text("Save", style: TextStyle(color: Colors.white)),
                  color: Colors.blue,
                  onPressed: () => this.submit()
                )
              ],
            ),
          )
      ),
    );
  }
}