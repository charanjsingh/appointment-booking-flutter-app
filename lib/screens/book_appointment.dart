import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:flutter_app/screens/my_app_drawer.dart';
import 'package:flutter_app/req/my_http_requests.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_app/req/globals.dart' as globals;
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;

class BookingData {
  DateTime    _selectedDate;
  String      _selectedTime;
  String      _serviceType;
}
BookingData instanceOne = new BookingData();

class SelectDate extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _SelectDateState();
}

class _SelectDateState extends State<SelectDate> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(title: Text("Book New Appointment")),
      drawer: MyAppDrawer(),
      body: Container(
        padding: EdgeInsets.only(left:15.0, right:15.0),
        child: globals.isLoggedIn ?
        CalendarCarousel(
          onDayPressed: (DateTime date) {
            this.setState(() {
              instanceOne._selectedDate = date;
              print(date);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SelectTime()),
              );
            });
          },
          thisMonthDayBorderColor: Colors.grey,
          selectedDateTime: instanceOne._selectedDate,
        ) :
            Container(
              alignment: AlignmentDirectional.center,
              padding: EdgeInsets.only(top: 30.0),
              child: Column(
                children: <Widget>[
                  Icon(FontAwesomeIcons.user, size: 80.0, color: Colors.black12),
                  Padding(
                    padding: EdgeInsets.only(top: 5.0),
                    child: Text("Please login to book an appointment",
                      style: TextStyle(
                        color: Colors.black38
                      ),
                    )
                  )
                ],
              ),
            )
      ),
    );
  }
}

class SelectTime extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _SelectTimeState();
}

class _SelectTimeState extends State<SelectTime> {
  
  List _availableTime;
  bool loading = false;

  @override
  @mustCallSuper
  void initState(){
    super.initState();
    loading = true;
    this.getData();
  }
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    if(loading == true){
      return Scaffold(
        appBar: AppBar(title: Text("Select Time")),
        drawer: MyAppDrawer(),
        body: Container(
          alignment: AlignmentDirectional.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
              Padding(
                padding: EdgeInsets.only(top:10.0),
                child: Text("Getting free time slots...")
              )
            ],
          )
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(title: Text("Select Time")),
      drawer: MyAppDrawer(),
      body: Container(
        child: ListView.builder(
          itemCount: _availableTime == null ? 0 : _availableTime.length,
          itemBuilder: (BuildContext context, int index){
            return new ListTile(
              title: Text(_availableTime[index]["title"]),
              onTap: (){
                instanceOne._selectedTime = _availableTime[index]["title"];
                print(_availableTime[index]["title"]);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => OtherDetails()),
                );
              },
            );
          },
        )
      ),
    );
  }

  Future<String> getData() async {
    String response = await httpGet("time.php");
    print('Something went wrong. \nResponse Code : $response');
    this.setState(() {
      _availableTime = json.decode(response);
      loading = false;
      print(_availableTime);
    });
    return null;
  }
}

class OtherDetails extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _OtherDetailsState();
}

class _OtherDetailsState extends State<OtherDetails> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<String> dropDownList = ['Nail Art', 'Nail Extension'];

  @override
  @mustCallSuper
  void initState(){
    super.initState();
    instanceOne._serviceType = "Nail Art";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(title: Text("Other Details")),
      drawer: MyAppDrawer(),
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Form(
          key: this._formKey,
          child: ListView(
            children: <Widget>[
              TextFormField(
                initialValue: instanceOne._selectedDate.toString(),
                decoration: InputDecoration(
                  labelText: "Appointment Date"
                ),
              ),
              TextFormField(
                initialValue: instanceOne._selectedTime,
                decoration: InputDecoration(
                  labelText: "Appointment Time"
                ),
              ),
              DropdownButton(
                  items: dropDownList.map((String value){
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  value: instanceOne._serviceType,
                  onChanged: (selectedValue){
                    this.setState((){
                      instanceOne._serviceType = selectedValue;
                    });
                  }
              ),
              RaisedButton(
                child: Text("Save"),
                onPressed: (){
                  print(instanceOne._serviceType);
                  print(instanceOne._selectedTime);
                  print(instanceOne._selectedDate);
                }
              )
            ],
          ),
        )
      ),
    );
  }
}
