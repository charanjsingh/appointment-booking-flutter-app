import 'package:flutter/material.dart';
import 'package:flutter_app/screens/my_app_drawer.dart';
import 'package:flutter_app/screens/my_bottom_app_bar.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      drawer: MyAppDrawer(),
      bottomNavigationBar: MyBottomAppBar(),
      body: Container(
        alignment: AlignmentDirectional.center,
        child: Text("Welcome")
      ),
    );
  }
}