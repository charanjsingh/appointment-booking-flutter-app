import 'package:flutter/material.dart';
import 'package:flutter_app/req/globals.dart' as globals;
import 'package:flutter_app/req/my_authenticate_requests.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyAppDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _MyAppDrawerState();
}

class _MyAppDrawerState extends State<MyAppDrawer> {

  final List<Widget> _drawerItems = [];

  @override
  void initState(){
    super.initState();
    if(globals.isLoggedIn){
      _drawerItems.add(
          UserAccountsDrawerHeader(
            accountName: Text(globals.loggedInUserName),
            accountEmail: Text(globals.loggedInUserMobile),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage("https://graph.facebook.com/v3.1/10214963334371448/picture"),
            ),
            otherAccountsPictures: <Widget>[
              Icon(FontAwesomeIcons.fire, color: Colors.white),
            ],
          )
      );
    }
    else {
      _drawerItems.add(
        Container(
          alignment: AlignmentDirectional.center,
          padding: EdgeInsets.only(top: 50.0, bottom: 30.0),
          decoration: BoxDecoration(
            color: Colors.blue
          ),
          child: Icon(FontAwesomeIcons.fire, size: 105.0, color: Colors.white),
        )
      );
    }
    _drawerItems.add(
      ListTile(
        leading: Icon(Icons.home),
        title: Text("Home"),
        onTap: () {
          Navigator.of(context).pushReplacementNamed('/home');
        },
      )
    );
    _drawerItems.add(
      ListTile(
        leading: Icon(Icons.event_available),
        title: Text("Book New Appointment"),
        onTap: () {
          Navigator.of(context).pushReplacementNamed('/book_appointment');
        },
      )
    );
    if(globals.isLoggedIn){
      _drawerItems.add(
        ListTile(
          leading: Icon(Icons.event_note),
          title: Text("My Appointments"),
          onTap: () {},
        )
      );
      _drawerItems.add(
        ListTile(
          leading: Icon(Icons.supervised_user_circle),
          title: Text("My Profile"),
          onTap: () => Navigator.of(context).pushReplacementNamed("/profile"),
        )
      );
      _drawerItems.add(
        ListTile(
          leading: Icon(Icons.exit_to_app),
          title: Text("Log Out"),
          onTap: () {
            setLoggedOut().then((result){
              if(result){
                globals.isLoggedIn = false;
                Navigator.of(context).pushReplacementNamed("/login");
              }
            });
          },
        )
      );
    }
    else{
      _drawerItems.add(
        ListTile(
          leading: Icon(Icons.power_settings_new),
          title: Text("Login"),
          onTap: () {
            Navigator.popUntil(context, ModalRoute.withName('/login'));
          },
        )
      );
    }
    _drawerItems.add(
      Divider(color: Colors.black38)
    );
    _drawerItems.add(
      Container(
        alignment: AlignmentDirectional.centerStart,
        padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Contact Us:"),
            Text("+91 98765-43210"),
          ],
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
      child: Column(
      children: _drawerItems
    ));
  }
}
