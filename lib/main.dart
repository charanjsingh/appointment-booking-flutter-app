import "package:flutter/material.dart";
import 'package:flutter_app/screens/login.dart';
import 'package:flutter_app/screens/home.dart';
import 'package:flutter_app/screens/book_appointment.dart';
import 'package:flutter_app/req/my_authenticate_requests.dart';
import 'package:flutter_app/req/globals.dart' as globals;

void main() async {

  await isLoggedIn().then((result){
    if(result != false){
      print("worked in main.dart");
      globals.isLoggedIn = true;
      globals.loggedInUserName = result.name;
      globals.loggedInUserMobile = result.mobile;
    }
  });
  print("running material app");
  runApp(MaterialApp(
      title: "UI Widgets",
      theme: ThemeData(
        fontFamily: "Roboto",
        primaryColor: Colors.blue,
      ),
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => new Login(),
        '/profile': (BuildContext context) => new MyProfile(),
        '/home': (BuildContext context) => new Home(),
        '/book_appointment': (BuildContext context) => new SelectDate(),
      },
      home: globals.isLoggedIn ? Home() : Login()
  ));
}
